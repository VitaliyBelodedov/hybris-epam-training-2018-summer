package org.training.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.training.dao.CustomUserDAO;
import javax.annotation.Resource;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class UserDAOIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private CustomUserDAO userDAO;
    @Resource
    private ModelService modelService;

    private static final String USER_UID = "15";
    private static final String USER_NAME = "user1";
    private static final String USER_PASSWORD = "12345";

    @Test
    public void userDAOTest() {
        List<UserModel> customers = userDAO.findAllUsers();
        final int size = customers.size();

        final UserModel userModel = new UserModel();
        userModel.setUid(USER_UID);
        userModel.setName(USER_NAME);
        userModel.setPassword(USER_PASSWORD);
        modelService.save(userModel);

        customers = userDAO.findAllUsers();
        assertEquals(size + 1, customers.size());
    }
}
