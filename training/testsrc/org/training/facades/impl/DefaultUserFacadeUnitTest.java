package org.training.facades.impl;

import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.training.data.UserData;
import org.training.service.CustomUserService;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultUserFacadeUnitTest {
    private DefaultCustomUserFacade customUserFacade;
    private CustomUserService customUserService;
    private UserModel userModel;

    private final String USER_ID = "15";
    private final String USER_NAME = "admin1";

    @Before
    public void setUp() {
        customUserFacade = new DefaultCustomUserFacade();
        customUserService = mock(CustomUserService.class);
        customUserFacade.setCustomUserService(customUserService);
        userModel = new UserModel();
        userModel.setUid(USER_ID);
        userModel.setName(USER_NAME);
    }

    @Test
    public void testGetAllUsers() {
        final List<UserModel> userModels = Arrays.asList(userModel);
        when(customUserService.getAllUsers()).thenReturn(userModels);

        final List<UserData> dto = customUserFacade.getAllUsers();

        assertNotNull(dto);
        assertEquals(userModels.size(), dto.size());
        assertEquals(userModel.getName(), dto.get(0).getName());
        assertEquals(userModel.getUid(), dto.get(0).getUid());
    }
}
