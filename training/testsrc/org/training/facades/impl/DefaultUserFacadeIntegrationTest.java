package org.training.facades.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.training.data.UserData;
import org.training.facades.CustomUserFacade;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import javax.annotation.Resource;
import java.util.List;

@IntegrationTest
public class DefaultUserFacadeIntegrationTest extends ServicelayerTransactionalTest {
    @Resource
    private CustomUserFacade customUserFacade;

    @Resource
    private ModelService modelService;

    private UserModel userModel;
    private final String USER_ID = "15";
    private final String USER_NAME = "admin1";

    @Before
    public void setUp() {
        userModel = new UserModel();
        userModel.setUid(USER_ID);
        userModel.setName(USER_NAME);
    }

    @Test
    public void testUserFacade() {
        List<UserData> userListData = customUserFacade.getAllUsers();
        assertNotNull(userListData);
        final int size = userListData.size();
        modelService.save(userModel);

        userListData = customUserFacade.getAllUsers();
        assertNotNull(userListData);
        assertEquals(size + 1, userListData.size());
        assertEquals(USER_ID, userListData.get(size).getUid());
        assertEquals(USER_NAME, userListData.get(size).getName());
    }
}
