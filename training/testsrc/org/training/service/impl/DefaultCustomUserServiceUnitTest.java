package org.training.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.training.dao.CustomUserDAO;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;

@UnitTest
public class DefaultCustomUserServiceUnitTest {

    private CustomUserServiceImpl customUserService;
    private CustomUserDAO userDAO;
    private UserModel userModel;

    private final String USER_ID = "15";
    private final String USER_NAME = "admin1";

    @Before
    public void setUp() {
        customUserService = new CustomUserServiceImpl();
        userDAO = mock(CustomUserDAO.class);
        customUserService.setUserDAO(userDAO);
        userModel = new UserModel();
        userModel.setUid(USER_ID);
        userModel.setName(USER_NAME);
    }

    @Test
    public void testGetAllUsers() {
        final List<UserModel> userModels = Arrays.asList(userModel);
        when(userDAO.findAllUsers()).thenReturn(userModels);
        final List<UserModel> result = customUserService.getAllUsers();
        assertEquals(1, result.size());
        assertEquals(userModel, result.get(0));
    }
}
