package org.training.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.training.service.CustomUserService;
import static org.junit.Assert.assertEquals;
import javax.annotation.Resource;
import java.util.List;

@IntegrationTest
public class DefaultUserServiceIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private CustomUserService customUserService;
    @Resource
    private ModelService modelService;

    private UserModel userModel;

    @Before
    public void setUp() {
        userModel = new UserModel();
        userModel.setUid("15");
        userModel.setName("FirstName");
        userModel.setPassword("1234");
    }

    @Test
    public void testUserService() {
        List<UserModel> users = customUserService.getAllUsers();
        final int size = users.size();

        modelService.save(userModel);
        users = customUserService.getAllUsers();
        assertEquals(size + 1, users.size());
    }
}
