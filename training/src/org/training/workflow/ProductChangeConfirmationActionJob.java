package org.training.workflow;

import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductChangeConfirmationActionJob implements AutomatedWorkflowTemplateJob {
    private Logger logger = LoggerFactory.getLogger(ProductChangeConfirmationActionJob.class);

    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        logger.info("Workflow message.");
        for (final WorkflowDecisionModel decision : workflowActionModel.getDecisions()) {
            return decision;
        }
        return null;
    }
}
