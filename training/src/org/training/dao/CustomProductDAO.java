package org.training.dao;

import de.hybris.platform.product.daos.ProductDao;

public interface CustomProductDAO extends ProductDao {
    Integer getCountOfProducts();
}
