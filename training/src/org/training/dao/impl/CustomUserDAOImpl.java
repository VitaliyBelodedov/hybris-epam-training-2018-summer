package org.training.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.training.dao.CustomUserDAO;

import java.util.List;

public class CustomUserDAOImpl extends DefaultUserDao implements CustomUserDAO {

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    private final String GET_ALL_USERS = String.format("SELECT {p: %s } FROM { %s AS p} ", UserModel.PK, UserModel._TYPECODE);

    @Override
    public List<UserModel> findAllUsers() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_USERS);
        return flexibleSearchService.<UserModel> search(query).getResult();
    }
}
