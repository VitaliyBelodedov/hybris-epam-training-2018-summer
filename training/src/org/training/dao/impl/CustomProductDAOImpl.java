package org.training.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.training.dao.CustomProductDAO;

public class CustomProductDAOImpl extends DefaultProductDao implements CustomProductDAO {
    @Autowired
    private FlexibleSearchService flexibleSearchService;
    private final String GET_ALL_PRODUCTS = String.format("SELECT COUNT(*) FROM {%s}", ProductModel._TYPECODE);

    public CustomProductDAOImpl(String typecode) {
        super(typecode);
    }

    @Override
    public Integer getCountOfProducts() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_PRODUCTS);
        query.setResultClassList(Lists.newArrayList(Integer.class));
        return (Integer) flexibleSearchService.search(query).getResult().get(0);
    }
}