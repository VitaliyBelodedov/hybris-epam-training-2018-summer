package org.training.dao;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;

import java.util.List;

public interface CustomUserDAO extends UserDao {

    List<UserModel> findAllUsers();
}
