package org.training.decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class AddSymbolToProductDescriptionDecorator implements CSVCellDecorator {

    @Override
    public String decorate(int position, Map<Integer, String> srcLine ) {
        String parsedValue=srcLine.get(position);
        return parsedValue + "_";
    }
}
