package org.training.decorators;

import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.impex.jalo.imp.ImpExImportReader;
import org.apache.log4j.Logger;
import org.training.constants.TrainingConstants;

@SystemSetup(extension = TrainingConstants.EXTENSIONNAME)
public class CustomInitUpdateProductService {
    private static final Logger LOG = Logger.getLogger(CustomInitUpdateProductService.class.getName());

    @SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = SystemSetup.Process.INIT)
    public void createProductsDuringInit() {
        Importer importer = new Importer(new ImpExImportReader(String.valueOf(ImpExManager.class.getResource("impex/custom-init-description.impex"))));
        try {
            importer.importAll();
        } catch (ImpExException e) {
            LOG.error(e);
        }
    }

    @SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = SystemSetup.Process.UPDATE)
    public void createProductsDuringUpdate() {
        Importer importer = new Importer(new ImpExImportReader(String.valueOf(ImpExManager.class.getResource("impex/custom-update-description.impex"))));
        try {
            importer.importAll();
        } catch (ImpExException e) {
            LOG.error(e);
        }
    }
}
