package org.training.attributes;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

public class CustomerDescriptionHandler extends
        AbstractDynamicAttributeHandler<String, CustomerModel> {
    @Override
    public String get(final CustomerModel model) {
        return String.format("%s : %s Order  quantity is %d", model.getName(), model.getContactEmail(), model.getOrders().size());
    }
}
