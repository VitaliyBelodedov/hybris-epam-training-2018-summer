package org.training.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.training.data.UserData;
import org.springframework.util.Assert;

public class UserPopulator implements Populator<UserModel, UserData> {

    @Override
    public void populate(UserModel source, UserData target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setUid(source.getUid());
        target.setName(source.getName());
    }
}
