package org.training.jobs;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import org.springframework.beans.factory.annotation.Required;
import org.training.dao.CustomProductDAO;

public class LogAmountOfProductJob extends AbstractJobPerformable<CronJobModel> {

    private CustomProductDAO productDAO;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        System.out.println("Amount of product : " + productDAO.getCountOfProducts());
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    @Required
    public void setProductDAO(final CustomProductDAO productDAO) {
        this.productDAO = productDAO;
    }
}
