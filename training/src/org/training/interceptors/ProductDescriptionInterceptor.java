package org.training.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductDescriptionInterceptor implements ValidateInterceptor<ProductModel> {
    private static final Logger LOG = Logger.getLogger(ProductDescriptionInterceptor.class.getName());

    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private WorkflowTemplateService workflowTemplateService;
    @Autowired
    private WorkflowProcessingService workflowProcessingService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelService modelService;

    @Override
    public void onValidate(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        if (productModel.getDescription().contains("_")) {
            LOG.info(String.format("Product model : %s have symbol '_' ", productModel.getCode()));
        }
        final WorkflowTemplateModel workflowTemplate = workflowTemplateService
                .getWorkflowTemplateForCode("ProductDataChange");
        final WorkflowModel workflow = workflowService.createWorkflow(workflowTemplate, userService.getAdminUser());
        modelService.save(workflow);
        for (final WorkflowActionModel action : workflow.getActions()) {
            modelService.save(action);
        }
        this.workflowProcessingService.startWorkflow(workflow);
    }

    public void setWorkflowService(final WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService) {
        this.workflowTemplateService = workflowTemplateService;
    }

    public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService) {
        this.workflowProcessingService = workflowProcessingService;
    }

    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
