package org.training.facades.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import org.training.data.UserData;
import org.training.facades.CustomUserFacade;
import org.training.service.CustomUserService;
import java.util.ArrayList;
import java.util.List;

public class DefaultCustomUserFacade implements CustomUserFacade {

    private CustomUserService customUserService;
    private Converter<UserModel, UserData> userConverter;

    @Required
    public void setUserConverter(final Converter<UserModel, UserData> userConverter) {
        this.userConverter = userConverter;
    }

    @Override
    public List<UserData> getAllUsers() {
        final List<UserModel> userModels = customUserService.getAllUsers();
        final List<UserData> userFacadeData = new ArrayList<>();
        for (final UserModel um : userModels) {
            userFacadeData.add(userConverter.convert(um));
        }
        return userFacadeData;
    }

    @Required
    public void setCustomUserService(CustomUserService customUserService) {
        this.customUserService = customUserService;
    }
}
