package org.training.facades;

import org.training.data.UserData;

import java.util.List;

public interface CustomUserFacade {
    public List<UserData> getAllUsers();
}
