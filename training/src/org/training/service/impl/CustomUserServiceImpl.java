package org.training.service.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import org.springframework.beans.factory.annotation.Required;
import org.training.dao.CustomUserDAO;
import org.training.service.CustomUserService;
import java.util.List;

public class CustomUserServiceImpl extends DefaultUserService implements CustomUserService {
    private CustomUserDAO userDAO;

    @Override
    public List<UserModel> getAllUsers() {
        return userDAO.findAllUsers();
    }

    @Required
    public void setUserDAO(final CustomUserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
