<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<title>Stadium Listing</title>
<body>
<h1>All Users</h1>
<ul>
    <c:forEach var="user" items="${users}">
        <li>${user.uid} - ${user.name}</li>
    </c:forEach>
</ul>
</body>
</html>