package org.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.training.data.UserData;
import org.training.facades.CustomUserFacade;

import java.util.List;

@Controller
public class UsersController {

    private CustomUserFacade customUserFacade;

    @RequestMapping(value = "/users")
    public String showAllUsers(final Model model) {
        final List<UserData> users = customUserFacade.getAllUsers();
        model.addAttribute("users", users);
        return "userListing";
    }

    @Autowired
    public void setUserFacade(final CustomUserFacade customUserFacade) {
        this.customUserFacade = customUserFacade;
    }
}
