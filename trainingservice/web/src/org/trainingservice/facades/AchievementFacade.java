/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.trainingservice.facades;

import java.util.List;
import org.trainingservice.data.AchievementData;


public interface AchievementFacade
{

	AchievementData getAchievement(String name);

	List<AchievementData> getAchievements();

}
