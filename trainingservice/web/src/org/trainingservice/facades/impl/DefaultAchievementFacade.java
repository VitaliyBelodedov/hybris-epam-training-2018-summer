/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.trainingservice.facades.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.access.annotation.Secured;
import org.trainingservice.data.AchievementData;
import org.trainingservice.facades.AchievementFacade;
import org.trainingservice.model.AchievementModel;
import org.trainingservice.services.AchievementService;


public class DefaultAchievementFacade implements AchievementFacade
{
	private AchievementService achievementService;

	private Converter<AchievementModel, AchievementData> achievementConverter;


	@Override
    @Secured("ROLE_CLIENT")
	public AchievementData getAchievement(final String name)
	{
		final AchievementModel achievement = getAchievementService().getAchievement(name);
		if (achievement != null) {
			return getAchievementConverter().convert(achievement);
		}
		else {
			return null;
		}
	}

	@Override
    @Secured("ROLE_CLIENT")
	public List<AchievementData> getAchievements() {
		final Collection<AchievementModel> achievementModels = getAchievementService().getAchievements();
		return achievementModels.stream().map(a -> getAchievementConverter().convert(a)).collect(Collectors.toList());
	}

    public AchievementService getAchievementService() {
        return achievementService;
    }

    @Required
    public void setAchievementService(AchievementService achievementService) {
        this.achievementService = achievementService;
    }

    public Converter<AchievementModel, AchievementData> getAchievementConverter() {
        return achievementConverter;
    }

    @Required
    public void setAchievementConverter(Converter<AchievementModel, AchievementData> achievementConverter) {
        this.achievementConverter = achievementConverter;
    }
}
