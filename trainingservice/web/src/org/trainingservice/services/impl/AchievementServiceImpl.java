package org.trainingservice.services.impl;

import org.springframework.beans.factory.annotation.Required;
import org.trainingservice.dao.AchievementDao;
import org.trainingservice.model.AchievementModel;
import org.trainingservice.services.AchievementService;
import java.util.List;

public class AchievementServiceImpl implements AchievementService {
    private AchievementDao achievementDao;

    @Override
    public AchievementModel getAchievement(String name) {
        return achievementDao.getAchievement(name);
    }

    @Override
    public List<AchievementModel> getAchievements() {
        return achievementDao.getAchievements();
    }

    @Required
    public void setAchievementDao(AchievementDao achievementDao) {
        this.achievementDao = achievementDao;
    }
}
