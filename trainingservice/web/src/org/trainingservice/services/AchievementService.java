package org.trainingservice.services;

import org.trainingservice.model.AchievementModel;

import java.util.List;

public interface AchievementService {
    AchievementModel getAchievement(String name);
    List<AchievementModel> getAchievements();
}
