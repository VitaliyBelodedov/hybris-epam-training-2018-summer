package org.trainingservice.controllers;

import de.hybris.platform.webservicescommons.mapping.DataMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trainingservice.data.AchievementData;
import org.trainingservice.data.AchievementDataList;
import org.trainingservice.dto.AchievementDTO;
import org.trainingservice.dto.AchievementsDTO;
import org.trainingservice.facades.impl.DefaultAchievementFacade;
import javax.annotation.Resource;
import java.util.List;
import static org.trainingservice.constants.TrainingserviceConstants.CLIENT_CREDENTIAL_AUTHORIZATION_NAME;
import static org.trainingservice.constants.TrainingserviceConstants.PASSWORD_AUTHORIZATION_NAME;

@Controller
@RequestMapping(value = "/")
public class DefaultController  {

    @Resource(name = "achievementFacade")
    private DefaultAchievementFacade achievementFacade;

    @Resource(name = "dataMapper")
    private DataMapper dataMapper;


    @RequestMapping(value = "/achievements", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get achievements", notes = "method returning achievements list.", produces = "application/json,application/xml" , authorizations =
	{ @Authorization(value = CLIENT_CREDENTIAL_AUTHORIZATION_NAME), @Authorization(value = PASSWORD_AUTHORIZATION_NAME) })
    public AchievementsDTO getAchievements()
    {
        List<AchievementData> achievementData = achievementFacade.getAchievements();
        AchievementDataList achievementDataList = new AchievementDataList();
        achievementDataList.setAchievements(achievementData);
        return dataMapper.map(achievementDataList, AchievementsDTO.class);
    }

    @RequestMapping(value = "/achievements/{name}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get achievement by name", notes = "method returning achievement.", produces = "application/json,application/xml" , authorizations =
            { @Authorization(value = CLIENT_CREDENTIAL_AUTHORIZATION_NAME), @Authorization(value = PASSWORD_AUTHORIZATION_NAME) })
    public AchievementDTO getAchievement(@ApiParam(value = "User identifier", required = true) @PathVariable final String name)
    {
        return dataMapper.map(achievementFacade.getAchievement(name), AchievementDTO.class);
    }

}
