package org.trainingservice.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.trainingservice.dao.AchievementDao;
import org.trainingservice.model.AchievementModel;
import java.util.List;

public class DefaultAchievementDao implements AchievementDao {

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    private final String GET_ALL_ACHIEVEMENTS = String.format("SELECT {p: %s} FROM {%s As p}", AchievementModel.PK, AchievementModel._TYPECODE);
    private final String GET_ACHIEVEMENT_BY_NAME = String.format("SELECT {p: %s} FROM {%s As p} WHERE {p: %s} LIKE ?name", AchievementModel.PK, AchievementModel._TYPECODE, AchievementModel.NAME);

    @Override
    public AchievementModel getAchievement(String name) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ACHIEVEMENT_BY_NAME);
        query.addQueryParameter("name", name);
        query.setResultClassList(Lists.newArrayList(AchievementModel.class));
        AchievementModel achievementModel = (AchievementModel) flexibleSearchService.search(query).getResult().get(0);
        return achievementModel;
    }

    @Override
    public List<AchievementModel> getAchievements() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_ACHIEVEMENTS);
        return flexibleSearchService.<AchievementModel>search(query).getResult();
    }
}
