package org.trainingservice.dao;

import org.trainingservice.model.AchievementModel;
import java.util.List;

public interface AchievementDao {
    AchievementModel getAchievement(String name);
    List<AchievementModel> getAchievements();
}
