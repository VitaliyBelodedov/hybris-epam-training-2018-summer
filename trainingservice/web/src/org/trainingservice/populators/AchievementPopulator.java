/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.trainingservice.populators;

import de.hybris.platform.converters.Populator;
import org.springframework.util.Assert;
import org.trainingservice.data.AchievementData;
import org.trainingservice.model.AchievementModel;

public class AchievementPopulator implements Populator<AchievementModel, AchievementData> {

	@Override
	public void populate(final AchievementModel source, final AchievementData target) {
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setName(source.getName());
		target.setPrize(source.getPrize());
	}

}
