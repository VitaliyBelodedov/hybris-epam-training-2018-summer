package org.componentaddon.controllers.cms;

import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import org.apache.commons.lang.StringUtils;
import org.componentaddon.model.SimpleInfoComponentModel;
import org.customsite.storefront.controllers.ControllerConstants;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller("SimpleInfoComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.SimpleInfoComponent)
public class SimpleInfoComponentController extends AbstractCMSAddOnComponentController<SimpleInfoComponentModel> {

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final SimpleInfoComponentModel component)
    {
        model.addAttribute("title", component.getTitle());
        model.addAttribute("time", new Date().toString());
    }

}