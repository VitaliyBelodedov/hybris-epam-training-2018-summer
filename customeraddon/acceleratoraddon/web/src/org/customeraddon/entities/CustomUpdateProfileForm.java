package org.customeraddon.entities;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;

public class CustomUpdateProfileForm extends UpdateProfileForm {
    private String hobby;

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}
