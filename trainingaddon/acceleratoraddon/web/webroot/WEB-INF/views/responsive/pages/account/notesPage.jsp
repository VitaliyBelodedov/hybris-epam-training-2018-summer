<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-6">
            <spring:theme code="text.account.profile.updatePersonalDetails"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-lg col-md-6">
        <div class="account-section-content">
            <div class="account-section-form">
                <form:form action="notes" method="post" commandName="noteForm">

                    <formElement:formInputBox idKey="note.name" labelKey="note.name" path="name" inputCSS="text" mandatory="true"/>
                    <formElement:formInputBox idKey="note.value" labelKey="note.value" path="value" inputCSS="text" mandatory="true"/>
                    <formElement:formCheckbox idKey="note.isPrivate" labelKey="note.isPrivate" path="isPrivate" mandatory="true"/>

                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <spring:theme code="text.account.notes.addNote" text="Add Note"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                    <button type="button" class="btn btn-default btn-block backToHome">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </div>
                </form:form>

                <h2><spring:theme code="text.account.profile.notes" text="Your Notes"/></h2>

                <table>
                    <tr><th>Name</th><th>Value</th><th>Private</th></tr>
                    <c:forEach items="${customerNotes}" var="item">
                        <tr><td>${item.name}</td> <td>${item.value}</td> <td>${item.isPrivate}</td></tr>
                    </c:forEach>
                </table>

                <div>
                    <h2><spring:theme code="text.account.profile.allCustomers" text="All Customers"/></h2>

                    <form:form action="anotherNotes" method="get" commandName="customersForm">

                    <select name="customerUid" id="customerUid">
                        <c:forEach items="${customers}" var="customer">
                            <option value="${customer}">${customer}</option>
                        </c:forEach>
                    </select>


                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <spring:theme code="text.account.profile.getCustomerNotes" text="Get Notes"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </form:form>

                </div>
                <div>

                    <h2><spring:theme code="text.account.profile.anotherCustomerNotes" text="Notes of"/>  ${anotherCustomerUid}</h2>

                    <table>
                        <tr><th>Name</th><th>Value</th><th>Private</th></tr>
                        <c:forEach items="${anotherCustomerNotes}" var="item">
                            <tr><td>${item.name}</td> <td>${item.value}</td> <td>${item.isPrivate}</td></tr>
                        </c:forEach>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>