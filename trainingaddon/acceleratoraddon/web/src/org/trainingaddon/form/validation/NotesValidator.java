package org.trainingaddon.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trainingaddon.form.NoteForm;

@Component("notesValidator")
public class NotesValidator implements Validator
{
    @Override
    public boolean supports(final Class<?> aClass)
    {
        return UpdateProfileForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors)
    {
        final NoteForm noteForm = (NoteForm) object;
        final String name = noteForm.getName();
        final String value = noteForm.getValue();


        if (StringUtils.isEmpty(name))
        {
            errors.rejectValue("name", "profile.note.name.invalid");
        }
        else if (StringUtils.length(name) > 255)
        {
            errors.rejectValue("name", "profile.note.name.invalid");
        }

        if (StringUtils.isBlank(value))
        {
            errors.rejectValue("value", "profile.note.value.invalid");
        }
        else if (StringUtils.length(value) > 255)
        {
            errors.rejectValue("value", "profile.note.value.invalid");
        }
    }

}
