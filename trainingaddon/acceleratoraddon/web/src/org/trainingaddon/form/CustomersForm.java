package org.trainingaddon.form;

public class CustomersForm {
    private String customerUid;

    public String getCustomerUid() {
        return customerUid;
    }

    public void setCustomerUid(String customerUid) {
        this.customerUid = customerUid;
    }
}
