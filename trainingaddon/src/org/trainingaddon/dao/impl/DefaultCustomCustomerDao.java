package org.trainingaddon.dao.impl;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.trainingaddon.dao.CustomCustomerDao;
import org.assertj.core.util.Lists;
import java.util.List;

public class DefaultCustomCustomerDao implements CustomCustomerDao {

    @Autowired
    private FlexibleSearchService flexibleSearchService;
    private final String GET_CUSTOMERS_UID = String.format("SELECT {p:%s} FROM { %s As p}", CustomerModel.UID, CustomerModel._TYPECODE);

    @Override
    public List<String> getAllCustomersUId() {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_CUSTOMERS_UID);
        query.setResultClassList(Lists.newArrayList(String.class));
        return flexibleSearchService.<String>search(query).getResult();
    }

}
