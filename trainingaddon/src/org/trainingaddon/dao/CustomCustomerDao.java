package org.trainingaddon.dao;

import de.hybris.platform.core.model.user.CustomerModel;
import org.trainingaddon.model.NoteModel;

import java.util.List;

public interface CustomCustomerDao {
    List<String> getAllCustomersUId();
}
