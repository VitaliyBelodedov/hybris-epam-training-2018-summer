package org.trainingaddon.facades;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import org.trainingaddon.data.NoteData;
import org.trainingaddon.model.NoteModel;

import java.util.List;

public interface CustomCustomerFacade extends CustomerFacade {
    List<String> getAllCustomersUId();
}
