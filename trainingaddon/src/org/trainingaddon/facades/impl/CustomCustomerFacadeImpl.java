package org.trainingaddon.facades.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import org.springframework.beans.factory.annotation.Required;
import org.trainingaddon.facades.CustomCustomerFacade;
import org.trainingaddon.services.CustomCustomerService;
import java.util.List;

public class CustomCustomerFacadeImpl extends DefaultCustomerFacade implements CustomCustomerFacade {
    private CustomCustomerService customCustomerService;

    @Override
    public List<String> getAllCustomersUId() {
        return customCustomerService.getAllCustomersUId();
    }

    @Required
    public void setCustomCustomerService(CustomCustomerService customCustomerService) {
        this.customCustomerService = customCustomerService;
    }
}
