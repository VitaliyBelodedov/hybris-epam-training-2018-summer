package org.trainingaddon.services.impl;

import org.springframework.beans.factory.annotation.Required;
import org.trainingaddon.dao.CustomCustomerDao;
import org.trainingaddon.services.CustomCustomerService;

import java.util.List;

public class CustomCustomerServiceImpl implements CustomCustomerService {
    private CustomCustomerDao customerDao;

    @Override
    public List<String> getAllCustomersUId() {
        return customerDao.getAllCustomersUId();
    }

    @Required
    public void setCustomerDao(CustomCustomerDao customerDao) {
        this.customerDao = customerDao;
    }
}
