package org.trainingaddon.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.util.Assert;
import org.trainingaddon.data.NoteData;
import org.trainingaddon.model.NoteModel;

public class NoteDataReversePopulator implements Populator<NoteData, NoteModel> {

    @Override
    public void populate(NoteData source, NoteModel target) throws ConversionException {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setName(source.getName());
        target.setValue(source.getValue());
        target.setIsPrivate(source.getIsPrivate());
    }
}
