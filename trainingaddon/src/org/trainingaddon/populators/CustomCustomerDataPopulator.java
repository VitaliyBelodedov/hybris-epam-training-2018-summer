package org.trainingaddon.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import org.trainingaddon.data.NoteData;
import org.trainingaddon.model.NoteModel;

import java.util.ArrayList;

public class CustomCustomerDataPopulator extends CustomerPopulator {
    private Converter<NoteModel, NoteData> noteConverter;

    @Override
    public void populate(final CustomerModel source, final CustomerData target) {
        super.populate(source, target);
        target.setNotes(new ArrayList<NoteData>(Converters.convertAll(source.getNotes(), noteConverter)));
    }

    @Required
    public void setNoteConverter(Converter<NoteModel, NoteData> noteConverter) {
        this.noteConverter = noteConverter;
    }
}
