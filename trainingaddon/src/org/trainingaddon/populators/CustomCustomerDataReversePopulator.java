package org.trainingaddon.populators;

import com.thoughtworks.xstream.converters.ConversionException;
import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;
import org.trainingaddon.data.NoteData;
import org.trainingaddon.model.NoteModel;

public class CustomCustomerDataReversePopulator extends CustomerReversePopulator {
    private Converter<NoteData, NoteModel> noteReverseConverter;

    @Override
    public void populate(final CustomerData source, final CustomerModel target) throws ConversionException {
        super.populate(source, target);
        target.setNotes(Converters.convertAll(source.getNotes(), noteReverseConverter));
    }

    @Required
    public void setNoteReverseConverter(Converter<NoteData, NoteModel> noteReverseConverter) {
        this.noteReverseConverter = noteReverseConverter;
    }
}
