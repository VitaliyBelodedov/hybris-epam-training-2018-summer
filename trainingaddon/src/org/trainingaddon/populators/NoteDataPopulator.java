package org.trainingaddon.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.trainingaddon.data.NoteData;
import org.trainingaddon.model.NoteModel;

public class NoteDataPopulator implements Populator<NoteModel, NoteData> {

    @Override
    public void populate(NoteModel source, NoteData target) throws ConversionException {
        target.setName(source.getName());
        target.setValue(source.getValue());
        target.setIsPrivate(source.getIsPrivate());
    }
}
